# living utopia IT
hier gibts eine kleine Übersicht über alle living utopia relevanten Dienste, die wir am laufen haben.

## Zugangsdaten
- Alle Zugangsdaten befinden sich in der Datei passwords.txt.enc
- Diese können mit folgenden Befehl entschlüsselt werden:
`openssl aes-256-cbc -d -a -in passwords.md.enc -out passwords.md`
- Nach Änderungen wieder verschlüsselt werden sie mit `openssl aes-256-cbc -a -salt -in passwords.md -out passwords.md.enc`
- Das nötige Passwort dazu bitte erfragen: [chandi@livingutopia.org](mailto:chandi@livingutopia.org)


## Hoster
- Die beiden Server `avocade.chandi.it` & `birke.chandi.it` laufen über chandi's netcup-Account
- Hauptsächlich alle Wordpress-Seiten und die Meisten E-Mail-Adressen (exklusive @livingutopia.org) auf greensta.de

## Domains (DNS)
- livingutopia.org läuft über raphi's domainfactory account `heilpsychologie.de`
- alwizuko.de, utopival.de, naiv-kollektiv.org über chandi's internetbs.net-Account `a.langecker@posteo.de`
- alle anderne über greensta


## @livingutopia.org-Emails
_auf `lumails.perseus.uberspace.de`_
- Verwaltet werden diese über die uberspace-Admin-Oberfläche unter https://uberspace.de/login

## Forum
_auf `chandi.serpens.uberspace.de` wird aber bald auf `avocado.chandi.it` umgezogen_
- Das Forum läuft auf [NodeBB](https://docs.nodebb.org/)
- es gibt Kategorien & Benutzergruppen.  Kategorien sind auf der Startseite und beinhalten die Diskussionen. Die Zugangsrechte für einzelne Kategorien können für jede Benutzergruppe individuell eingestellt werden, was es ermöglicht, zB eine Kategorie für alle sichtbar zu machen, aber nur eine Gruppe darf darin auch schreiben.

## Cloud
_auf `birke.chandi.it`_
- nutzt _Nextcloud_
- Updates über die Weboberfläche brechen manchmal ab => lieber per SSH `sudo -u lu-cloud php /var/www/cloud.livingutopia.org/htdocs/updater/updater.phar`

## Newslettersystem
_auf `birke.chandi.it`_
- https://mails.livingutopia.org/
- nutzt _mailtrain_
- **Wichtig:** Abonnierte werden derzeit noch nicht automatisch eingetragen. Diese müssen manuell aus Wordpress exportiert und in mailtrain importiert werden.
- es werden mehrere mailgun-Accounts verwendet, über welche die Emails mithilfe eines Loadbalancers verteilt werden. `node /opt/smtpbalance/smtpbalance.js` sollte laufen (aktuell unter tmux)


## MOVE-Openproject
_auf `birke.chandi.it`_
- ...

## utopival Webseite
_auf `birke.chandi.it`_
- https://gitlab.com/livingutopia/utopival.de
- ...






